﻿/*
* CRM_Customer.cs
*
* 功 能： N/A
* 类 名： CRM_Customer
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2016-02-29 11:53:22    黄润伟    
*
* Copyright © 2015 www.xhdcrm.com All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
    /// <summary>
    /// 数据访问类:CRM_Customer
    /// </summary>
    public partial class CRM_Customer
    {
        public CRM_Customer()
        { }
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from CRM_Customer");
            strSql.Append(" where id=@id ");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50)           };
            parameters[0].Value = id;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(XHD.Model.CRM_Customer model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into CRM_Customer(");
            strSql.Append("id,cus_name,cus_tel,Remarks,emp_id,create_id,create_time)");
            strSql.Append(" values (");
            strSql.Append("@id,@cus_name,@cus_tel,@Remarks,@emp_id,@create_id,@create_time)");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50),                   
                    new SqlParameter("@cus_name", SqlDbType.VarChar,250),
                    new SqlParameter("@cus_tel", SqlDbType.VarChar,250),
                    new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
                    new SqlParameter("@emp_id", SqlDbType.VarChar,50),
                    new SqlParameter("@create_id", SqlDbType.VarChar,50),
                    new SqlParameter("@create_time", SqlDbType.DateTime)};
            parameters[0].Value = model.id;
            parameters[1].Value = model.cus_name;
            parameters[2].Value = model.cus_tel;
            parameters[3].Value = model.Remarks;
            parameters[4].Value = model.emp_id;
            parameters[5].Value = model.create_id;
            parameters[6].Value = model.create_time;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(XHD.Model.CRM_Customer model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CRM_Customer set ");
            strSql.Append("cus_name=@cus_name,");
            strSql.Append("cus_tel=@cus_tel,");
            strSql.Append("Remarks=@Remarks,");
            strSql.Append("emp_id=@emp_id");
            strSql.Append(" where id=@id ");
            SqlParameter[] parameters = {                   
                    new SqlParameter("@cus_name", SqlDbType.VarChar,250),
                    new SqlParameter("@cus_tel", SqlDbType.VarChar,250),
                    new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
                    new SqlParameter("@emp_id", SqlDbType.VarChar,50),
                    new SqlParameter("@id", SqlDbType.VarChar,50)};
            parameters[0].Value = model.cus_name;
            parameters[1].Value = model.cus_tel;
            parameters[2].Value = model.Remarks;
            parameters[3].Value = model.emp_id;
            parameters[4].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CRM_Customer ");
            strSql.Append(" where id=@id ");
            SqlParameter[] parameters = {
                    new SqlParameter("@id", SqlDbType.VarChar,50)           };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CRM_Customer ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,cus_name,cus_tel,Remarks,emp_id,create_id,create_time ");           
            strSql.Append(",(select dep_name from hr_department where id = (select dep_id from hr_employee where id = CRM_Customer.emp_id)) as department");
            strSql.Append(",(select name from hr_employee where id = CRM_Customer.emp_id) as employee");
            strSql.Append(" FROM CRM_Customer ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" id,cus_name,cus_tel,Remarks,emp_id,create_id,create_time ");           
            strSql.Append(",(select dep_name from hr_department where id = (select dep_id from hr_employee where id = CRM_Customer.emp_id)) as department");
            strSql.Append(",(select name from hr_employee where id = CRM_Customer.emp_id) as employee");
            strSql.Append(" FROM CRM_Customer ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            StringBuilder strSql_inner = new StringBuilder();
            StringBuilder strSql_grid = new StringBuilder();
            StringBuilder strSql_total = new StringBuilder();

            //联合数据
            strSql_inner.Append("( ");
            strSql_inner.Append("   SELECT  ");
            strSql_inner.Append($"      ROW_NUMBER() OVER(ORDER BY {filedOrder}) AS n ");
            strSql_inner.Append("      ,CRM_Customer.[id] ");
            strSql_inner.Append("      , CRM_Customer.[cus_name] ");
            strSql_inner.Append("      , CRM_Customer.[cus_tel] ");
            strSql_inner.Append("      , CRM_Customer.[Remarks] ");
            strSql_inner.Append("      , CRM_Customer.[emp_id] ");
            strSql_inner.Append("      , CRM_Customer.[create_id] ");
            strSql_inner.Append("      , CRM_Customer.[create_time] ");
            strSql_inner.Append("      , hr_employee.name ");
            strSql_inner.Append("      , hr_department.dep_name ");
            strSql_inner.Append("FROM[XHD_Auth].[dbo].[CRM_Customer] ");
            strSql_inner.Append("  INNER JOIN hr_employee ON hr_employee.id = CRM_Customer.emp_id ");
            strSql_inner.Append("  INNER JOIN hr_department ON hr_employee.dep_id = hr_department.id ");

            if (strWhere.Trim() != "")
            {
                strSql_inner.Append(" WHERE " + strWhere);
            }
            strSql_inner.Append(") w1 ");

            //Total数据
            strSql_total.Append(" SELECT COUNT(ID) FROM ");
            strSql_total.Append(strSql_inner.ToString());

            //grid数据
            strSql_grid.Append("SELECT * FROM ");
            strSql_grid.Append(strSql_inner.ToString());
            strSql_grid.Append("WHERE n BETWEEN " + PageSize * (PageIndex - 1) + " AND " + PageSize * PageIndex);

            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

